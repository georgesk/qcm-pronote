DESTDIR =
SUBDIRS = layouts

all:
	for d in $(SUBDIRS); do make -C $$d $@ DESTDIR=$(DESTDIR); done
clean:
	for d in $(SUBDIRS); do make -C $$d $@ DESTDIR=$(DESTDIR); done

install:
	for d in $(SUBDIRS); do make -C $$d $@ DESTDIR=$(DESTDIR); done

.PHONY: all clean install
