from PyQt5 import QtCore, QtGui, QtWidgets

class SourceView (QtWidgets.QTableView):
    nom_colonnes = ["Source", "Analyse", "Résultat"]
    def __init__ (self, parent=None):
        QtWidgets.QTableView.__init__(self, parent)
        self.model = QtGui.QStandardItemModel( 1, 3, self )
        for c in range(3):
            self.model.setHorizontalHeaderItem(
                c, QtGui.QStandardItem(self.nom_colonnes[c]) );
        self.setModel(self.model)
        return
