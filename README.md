QCM-PRONOTE
===========

Cette petite application a pour but de pouvoir exporter des QCM créés 
et partagés par des utilisateurs de Pronote, vers diverses autres plateformes
proposant des exercices interactifs.

Plateformes supportées
----------------------

La version 0.1 de qcm-pronote supporte uniquement l'export vers le système
**Wims**.

Installation
------------

Il suffira peut-être de télécharger le contenu du dépôt et de le décomprimer
sur son disque dur. Cependant, la petite application qcm-pronote possède des
*dépendances*, c'est à dire que pour son fonctionnement, elle dépend d'autres
logiciels qui doivent être installés et configurés, sur le même ordinateur.

### Liste des dépendances de qcm-pronote

- Python, en version supérieure ou égale à 3.8.3
- PyQt5, une version récente doit faire l'affaire

Mode d'emploi
-------------

### Récupération de qcm partagés par des utilisateurs de Pronote

Le mieux consiste à créer un répertoire (dossier), dans le quel on
téléchargera les qcm qu'on veut exporter en une seule fois. Par exemple
on peut créer un dossier `data`.

Ensuite, on visite le site web https://www.index-education.com/fr/qcm-liste.php
![pagedetéléchargement](img/snap1.png)

On peut y sélectionner des qcm partagés en précisant le niveau d'enseignement,
la matière, et éventuellement des mots-clés.

Les liens de téléchargement permettent de récupérer des fichiers au format XML,
qu'on rassemblera dans le dossier `data`, si c'est ce dossier qu'on veut
utiliser.

### Génération d'un module Wims contenant les QCM

Lancer le programme `qcm-convert`, par exemple par la commande
`python3 qcm-convert` ; pour fonctionner, cette application nécessite
que les *dépendances* soient installées.

Cliquer sur le bouton **Ajouter ...** et sélectionner un ou plusieurs
nouveaux fichiers :
![choix_des_fichiers](img/snap2.png)

On choisit un ou plusieurs fichiers, puis on clique sur le bouton
**Open** en bas à droite. Si les fichiers sont analysés avec succès,
ils apparaissent alors dans l'interface utilisateur du programme :
![fichiers_choisis](img/snap3.png)

Quand les fichiers choisis apparaissent, et que la colonne d'analyse contient
"ok" pour chacun, on peut alors générer un module Wims. Pour cela, cliquer sur
le bouton **Générer ...**

Un premier dialogue apparaît, dans le quel on peut préciser le nom du module
pour Wims ; il y a quatre données à préciser,

- la **zone**, à choisir parmi test, local, H1, H2, etc. cette zone correspond
  à une classification des niveaux pour Wims
- le **domaine**, à choisir parmi physics, chemistry, lang, mathematics.
- le **nom** lui-même, qu'on peut choisir librement 
  (ne pas utiliser d'espace ni de ponctuation)
- la **langue**, par défaut c'est *fr* pour le français.

![nommage-wims](img/snap4.png)

Renseigner le nom du module, puis cliquer sur le bouton **OK**.

Un nouveau dialogue apparaît ensuite, pour choisir un dossier de destination
On peut choisir un dossier-cible comme `/tmp` par exemple. Quand le 
dossier-cible est activé, cliquer sur le bouton en bas à droite, **Open**.

Et voilà ! avec l'exemple illustré ci-dessus (on ne renseigne pas le nom
de module), ça crée un fichier dont le chemin complet est :
`/tmp/modtool-test~physics~sansNom.fr.tgz`

On remarque que l'absence de nom a été palliée d'une certaine façon.

### Récupération du module, dans Wims

1. Ouvrir dans un navigateur web un service Wims fourni par un ordinateur
   distant ou local
2. Dans la session Wims ainsi initiée, dérouler le menu 
   **Création de ressource** et cliquer sur **Modtool**. Il faut alors
   s'identifier (pour disposer d'un compte de *développeur modtool*, il faut
   demander sa création au webmestre du site
3. Une fois qu'on est dans l'environnement « Modtool », cliquer sur le lien
   **Restauration** :  ![le_bouton_restauration](img/snap6.png)
4. Dans le formulaire de restauration, Cliquer sur le bouton **Parcourir...**
   et sélectionner le module récemment généré (on rappelle que pour l'exemple,
   il s'agit du fichier `/tmp/modtool-test~physics~sansNom.fr.tgz`). 
   Quand ce fichier est sélectionné, cliquer sur le bouton gris restangulaire
   **Restauration** un peu plus loin à droite.
5. Un nouveau formulaire apparaît, qui présente le module restauré ; il
   suffit alors de cliquer sur le bouton **Aller travailler sur le module**
   pour pouvoir le tester et l'améliorer.
   
### Tester et améliorer le module, dans Wims

Parmi les boutons d'action dans la nouvelle fenêtre qui apparaît, si on est
« allé travailler sur le module », il y a **Tester** qui permet de voir le
fonctionnement du module pour les élèves, dans une fenêtre qui surgira.

#### Améliorations possibles

Les seuls fichiers à « améliorer » sont les fichiers `data1.txt`, `data2.txt`,
etc. En effet, il n'est pas rare que des QCM partagés contiennent des fautes
d'une façon ou d'une autre. Ces fichiers sont dans un format de texte, à
éditer soit dans l'environnement Wims (boutons **Modifier** puis utilisation
de l'éditeur en ligne), soit à l'aide d'une application externe, toujours
de la même façon, mais en utilisant par exemple du copier-coller pour 
transférer de gros blocs de texte d'un seul coup.

#### Format des fichiers data*.txt pour les qcm

Chaque question est formée de plusieurs lignes conséctives :

1. la première ligne commence par le caractère deux-points `:` et contient
   le texte de la question qui sera posée ; le format HTML est autorisé ; 
   attention à bien tout marquer sur une même ligne, si on veut des sauts
   de lignes dans le rendu de la question, il suffit d'utiliser des balises
   HTML qui le permettent, comme la balise ```<br>```
2. le deuxième ligne peut contenir une phrase de feedback ; elle est
   optionnelle ; si un feedback est écrit, alors il sera affiché aux
   utilisateurs en cas de réponse fausse.
3. la troisième ligne (ou la deuxième en l'absence de feedback) doit contenir
   des nombres. Deux formats sont possibles, soit un nombre tout seul, et
   dans ce cas il s'agit d'une question ayant une et une seule bonne réponse,
   le nombre est le numéro de la bonne répoonse dans les lignes suivantes
   (le numéro 1 signifie que la bonne réponse vient tout de suite à la 
   ligne suivante, le numéro 2 que c'est la ligne d'après, etc.) ; ou alors,
   le format est une suite de nombres dont le total fait 100, par exemple,
   0,33,33,33 : ceci signifie qu'on a une question dont les bonnes réponses
   sont éventuellement multiples, que la première réponse qui suit est mauvaise,
   puis que les trois suivantes sont bonnes et apportent chacune un tiers de la
   note maximale.
4. les lignes suivantes sont les textes des propositions de réponses : une
   ligne par proposition de réponse, ni plus ni moins. On ne doit pas laisser
   de ligne vide !
   
