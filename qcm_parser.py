#! /usr/bin/python3

import sys, tarfile, io, os, hashlib, base64, unittest
from xml.dom import minidom, Node
from xml.dom.minicompat import NodeList

vraiequestion = set(("multichoice", "numerical"))

def img_to_wims(obj):
    """
    :return: code de l'élément IMG de l'objet, qui doit avoir un
      attribut self.image_filename
    """
    if not obj.image_filename:
        return ""
    return f"""<img src="$module_dir/images/{obj.image_filename}" alt="{obj.image_filename}"/>"""

def img_to_html(obj):
    """
    :return: code de l'élément IMG de l'objet, qui doit avoir un
      attribut self.image_base64
    """
    if not obj.image_base64:
        return ""
    return f"""<img src="data:image/png;base64,{obj.image_base64}" alt=""/>"""

def toLatin1(s):
    """
    Encode une chaîne en Latin-1, en évitant trop de déperditions
    car certains caractères utf-8 couramment utilisés par des auteurs
    n'ont pas de contrepartie en latin-1
    :param s: un texte
    :type  s: str
    :rtype: bytes
    """
    return s.encode("latin-1", errors='xmlcharrefreplace')


def hashedImageName(s):
    """
    Crée un nom de fichier PNG à partir du hash d'une chaîne de caractères
    :param s: la chaîne de caractères
    """
    hash = hashlib.md5(s.encode("ascii")).hexdigest()
    return f"image_{hash}.png"

def findText(elt, cumul=""):
    """
    Trouve le texte contenu dans un élément, soit dans un noeud texte,
    soit dans un noeud cdata

    :param elt: un noeud d'un arbre DOM, ou une instace de NodeList
    :return: le texte rassemblé dans une seule chaîne
    """
    initial = cumul == ""
    if isinstance(elt, NodeList):
        for n in elt:
            cumul = findText(n, cumul)
            if initial:
                return cumul.strip()
            else:
                return cumul
    elif elt.nodeType in (Node.TEXT_NODE, Node.CDATA_SECTION_NODE):
        cumul += " " + elt.data.strip()
        return cumul
    else:
        for n in elt.childNodes:
            cumul = findText(n, cumul)
        if initial:
            return cumul.strip().replace("\n"," ").replace("\r","")
        else:
            return cumul

class QcmReponse:
    """
    Support des réponses de QCM

    paramètres du constructeur

    :param elt: un éléent "answer" dans un arbre DOM
    """

    def __init__(self, elt):
        self.elt=elt
        self.fraction       = elt.getAttribute("fraction")
        self.text           = findText(elt.getElementsByTagName("text")[0])
        self.image_base64   = None
        self.image_decoded  = None
        self.image_filename = None
        self.feedback       = findText(elt.getElementsByTagName("feedback")[0])
        image = elt.getElementsByTagName("image_base64")
        if image:
            self.image_base64 = findText(image[0])
            self.image_decoded = base64.b64decode(self.image_base64.encode("ascii"))
            self.image_filename = hashedImageName(self.image_base64)
        return

    @property
    def to_wims_data(self):
        """
        transforme la réponse en une ligne que Wims 
        peut digérer à l'aide de CreateQCM
        """
        return f"""{self.text} {img_to_wims(self)}"""

    def toHTML(self):
        return f"""<li><span class="reponse">{self.text}</span> {img_to_html(self)}</li>"""
        
    
class QcmQuestion:
    """
    Support des questions de QCM

    Paramètres du constructeur :

    :param elt: un élément "question" dans un arbre DOM
    """
    def __init__(self, elt):
        self.elt = elt
        self.type = elt.getAttribute("type")
        self.name = None
        self.questiontext = None
        self.answers = []
        self.category = None
        self.single = None
        self.image_base64 = None
        self.image_decoded  = None
        self.image_filename = None
        self.externallink = None
        self.goodreply = -1
        if self.type == "category":
            self.category = elt.getElementsByTagName("category")[0]
            text = findText(self.category)
            d = minidom.parseString(text)
            self.name = findText(d.getElementsByTagName("name")[0])
            self.niveau = findText(d.getElementsByTagName("niveau")[0])
            self.matiere = findText(d.getElementsByTagName("matiere")[0])
        elif self.type in vraiequestion:
            self.name = findText(elt.getElementsByTagName("name")[0])
            self.questiontext = elt.getElementsByTagName("questiontext")[0]
            self.answers = [QcmReponse(a) for a in elt.getElementsByTagName("answer")]
            self.single = findText(elt.getElementsByTagName("single")) == "true"
            self.feedbacks = " ".join([a.feedback for a in self.answers])
            # image de la question : une seule image, descendante direct
            # s'il y en a!
            image =  [i for i in elt.getElementsByTagName("image_base64") if i.parentNode == elt]
            if image:
                self.image_base64 = findText(image[0])
                self.image_decoded = base64.b64decode(self.image_base64.encode("ascii"))
                self.image_filename = hashedImageName(self.image_base64)
            self.externallink = findText(elt.getElementsByTagName("externallink"))
            if self.single:
                self.goodreply = [n for n, a in enumerate(self.answers) if a.fraction == "100"][0]
            else: # plusieurs réponses sont à cocher
                # donc on n'enregistre pas le numéro de la bonne réponse mais
                # les valeurs de "fraction" de chaque réponse
                self.goodreply = [int(a.fraction) for a in self.answers]
        return

    @property
    def to_wims_data(self):
        """
        transforme la question en quelques lignes de données que Wims 
        peut digérer à l'aide de CreateQCM
        """
        result = ""
        # on met en place l'énoncé et le feedback pour tout type de
        # vraie question
        if self.type != "category":
            questionText = ""
            if self.questiontext:
                questionText = findText(self.questiontext)
            externallink = ""
            if self.externallink:
                externallink = f"""<div class="externallink"><a href="{self.externallink}" target="external">Cliquez ici</a></div>"""
            endl="\n"
            feedbacks = [a.feedback for a in self.answers]
            feedbacks={f for f in feedbacks}.difference({''})
            if feedbacks:
                feedbacks = " ".join(self.feedbacks) + endl
            else:
                feedbacks = ""
            if self.type in vraiequestion:
                result += f""":<h2 class="name">{self.name}</h2>{questionText} {externallink} {img_to_wims(self)}
{feedbacks}"""

        # dans le cas des QCMs, à une ou pluseirs réponses,
        # mise en place de la ligne d'analyse de réponse, qui est
        # « numérique au sens large » c'est à dire sans aucun caractère
        # [a-zA-Z]
        numbers = ""
        endl="\n"
        if self.type == "multichoice":
            if self.single:
                numbers = f"{self.goodreply + 1}\n"
            else:
                numbers = f"""{",".join([str(f) for f in self.goodreply])+endl}"""
        if self.type == "numerical":
            numbers = "||\n"
        result += numbers

        # pour toute vraie question, mie en place des lignes de
        # propositions de réponses
        reponses=[]
        if self.type != "category":
            reponses=[a.to_wims_data for a in self.answers]
        if self.type in vraiequestion:
            result += f"""{endl.join(reponses)}
"""
        return result

    def __str__(self):
        endl="\n"
        if self.type=="multichoice" and self.single:
            return f"""QCM à une bonne réponse : 
énonce :
{findText(self.questiontext)}
propositions (bonne réponse = {self.goodreply}) :
{endl.join([a.text for a in self.answers])}
"""
        elif self.type=="multichoice" and not self.single:
            return f"""QCM à plusieurs bonnes réponses : 
énoncé = 
{findText(self.questiontext)}
propositions = 
{endl.join([a.text for a in self.answers])}
"""
        elif self.type=="numerical":
            return f"""Question à réponse numérique :
énoncé =
{findText(self.questiontext)}
réponses acceptées =
{endl.join(set([a.text for a in self.answers]))}
"""
        elif self.type=="category":
            return f"""Catégorie : {findText(self.category)}""" 
        else:
            return f"Type de question non encore supporté : {self.type}"

        
    def toHTML(self):
        result = ""
        externallink = ""
        if self.externallink:
            externallink = f"""<div class="externallink"><a href="{self.externallink}" target="external">Cliquez ici</a></div>"""
        reponses=[a.toHTML() for a in self.answers]
        indent = "  \n"
        title=""
        if self.type=="multichoice":
            if self.single:
                title = "<h1>QCM à une bonne réponse</h1>"    
            else:
                title = "<h1>QCM à plusieurs bonnes réponses</h1>"
        elif self.type=="numerical":
            title = "<h1>Question à réponse numérique</h1>"
        if self.type in vraiequestion:
            result += f"""{title}<h2 class="name">{self.name}</h2>
<p>{findText(self.questiontext)}</p>{externallink}{img_to_html(self)}
"""
        if self.type=="multichoice":
            result += f"""<h3>propositions (bonne réponse = {self.goodreply}) :</h3>"""
        elif self.type=="numerical":
            result += "<h3>possibilités de réponses :</h3>"
        if reponses:
            result += f"""<ol>
  {indent.join(reponses)}
</ol>
"""
        elif self.type=="category":
            return f"""Catégorie : {findText(self.category)}""" 
        else:
            return f"Type de question non encore supporté : {self.type}"
        return result
    
class QCM:

    def __init__(self):
        self.questions=[]
        self.category = None
        return
    
    def __str__(self):
        endl="\n"
        les_questions=endl.join([str(n)+" "+str(q) for n, q in enumerate(self.questions)])
        return f"""QCM : {self.category.name} {self.category.niveau} {self.category.matiere} :
{len(self)} questions = {len([q for q in self.questions if q.single])} questions à une réponse, {len([q for q in self.questions if q.single==False])} questions à plusieurs réponses.
==============================================================
{les_questions}
"""

    def toHTML(self):
        endl="<br/>"
        les_questions = "<ol><li>" + "</li><li>".join([q.toHTML() for q in self.questions]) + "</li></ol>"
        return f"""<h1>QCM : {self.category.name} {self.category.niveau} {self.category.matiere}</h1>
<p>{len(self)} questions</p>
<ul>
  <li>{len([q for q in self.questions if q.single])} questions à une réponse,</li>
  <li> {len([q for q in self.questions if q.single==False])} questions à plusieurs réponses.</li>
</ul>
<p>
==============================================================
</p>
{les_questions}
"""

    def __len__(self):
        """renvoie le nombre de questions"""
        return len(self.questions)

    @property
    def to_wims_data(self):
        """
        transforme les questions en un fichier de données que Wims peut digérer
        à l'aide de CreateQCM
        """
        result=""
        for q in self.questions:
            result+=q.to_wims_data
        return result

    def addToTarfile(self, tar, dirname, ident):
        """
        ajoute un questionnaire et ses images.

        :param tar: un fichier tar en RAM
        :type  tar: tarfile.TarFile
        :param dirname: chemin du dossier où mettre le fichier
        :param ident: un signe distinctif, par exemple un numéro
        """
        f = io.BytesIO(toLatin1(self.to_wims_data))
        tarinfo = tarfile.TarInfo(name=os.path.join(dirname, f"data{ident+1}.txt"))
        tarinfo.size = len(f.getvalue())
        f.seek(0)
        tar.addfile(tarinfo, fileobj=f)
        # ajout des images
        for q in self.questions:
            images = q.elt.getElementsByTagName("image_base64")
            for i in images:
                image_base64 = findText(i)
                imgdata = base64.b64decode(image_base64.encode("ascii"))
                f = io.BytesIO(imgdata)
                fname = os.path.join(dirname, "images", hashedImageName(image_base64))
                tarinfo = tarfile.TarInfo(name=fname)
                tarinfo.size = len(f.getvalue())
                f.seek(0)
                tar.addfile(tarinfo, fileobj=f)
        return
         
 
    @staticmethod
    def from_pronote(filename):
        """
        Création d'une instance QCM à partir d'un fichier XML
        issu de pronote.

        :param filename: nom du fichier à lire
        :return: une instance de QCM
        """
        doc=minidom.parse(filename)
        qcm=QCM()
        qcm.questions = [QcmQuestion(q) for q in doc.getElementsByTagName("question")]
        qcm.category  = [q for q in qcm.questions if q.type == "category"][0]
        qcm.questions = [q for q in qcm.questions if q.type != "category"]
        return qcm
        
class Test(unittest.TestCase):
    fichier = ""
    default = ""
    defaultNbQuestions = 1
   
    def test_01minidom(self):
        doc=minidom.parse(self.fichier)
        self.assertIsInstance(doc, minidom.Document, msg = f"Impossible d'analyser le code XML de {self.fichier}")
        root = doc.documentElement
        self.assertEqual(root.tagName, "quiz", msg = "Pas trouvé la racine « quiz »")
        
    def test_02analyse_qcm(self):
        qcm = QCM.from_pronote(self.fichier)
        self.assertEqual(qcm.category.name, "Nom du QCM", msg = "impossible de retrouver le nom du QCM : échec de l'analyseur ?")
        if self.fichier == self.default:
            self.assertEqual(len(qcm), self.defaultNbQuestions, msg = "Le nombre de questions du QCM n'est pas {self.defaultNbQuestions}.")
            for i, a in enumerate(qcm.questions[0].answers):
                self.assertEqual(a.text, f"<div>réponse {i+1}</div>", msg = f"la réponse {i+1} n'est pas celle qu'on attend")
            self.assertEqual(qcm.questions[0].feedbacks, "feedback 1 feedback 2 feedback 3 feedback 4 feedback 5", msg = "On attendait un feedback différent")
            image_base64 = ""
            with open("test_data/image1.png", "rb") as image:
                image_base64 = base64.b64encode(image.read())
            self.assertEqual(qcm.questions[0].image_base64.replace(" ", "").encode("ascii"), image_base64, msg = "L'image encodée est incorrecte")
        return
        
if __name__ == "__main__":
    Test.default = os.path.join("test_data","t1.xml")
    Test.fichier = Test.default
    if len(sys.argv) > 1:
        Test.fichier = sys.argv.pop()
    unittest.main()
